# Backend CRUD API REST
 
_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._
 
## Comenzando 🚀
 
_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._
 
Ver **Deployment** para conocer cómo desplegar el proyecto.
 
 
### Pre-requisitos 📋
 
Para el correcto funiconamiento de las prácticas necesitaremos una maquina virtual, concretamenta la imagen ISO de Ubuntu, la cual podremos descargar desde la propia página web de ubuntu.
 
### Instalación 🔧
 
 Para poder llevar acabo la práctica necesitaremos instalar un editor de texto(en mi caso será visual studio code) y postman. Ambos programas se pueden instalar desde la propia terminal introduciendo las siguientes lineas de código.

 Para instalar Visual Studio Code tendremos que introducir en la terminal "sudo snap install --classic code".

 Para instalar Postman tendremos que introducir en la terminal "sudo snap install postman".

 A parte de estos programas necesitaremos instalar NodeJS para poder realizar la práctica, ya que es el gestor de paquetes.
 
 Para instalarlo debemos de introducir las siguientes líneas de codigo en la terminal:
    "sudo apt update"
    "sudo apt install npm"
    "sudo npm clean -f"
    "sudo npm i -g n"
Con introducir estas lineas ya tendremos a NodeJS en funcionamiento.

Una vez que tengamos todo lo anterior instalado tendremos que sincronizar nuestra maquina virtual con un repositorio en la nube, en este caso será Bitbucket.

## Ejecutando las pruebas ⚙️
 
Para comprobar si lo que hemos hcecho de la practica esta bien y funciona debemos ejecutar en la terminal el archivo Index.js. Haciendo esto, podremos visitar en nuestro navegador de confianza el trabajo que hemos realizado en http://localhost:3000 .
El numero que se encuentra en el enlace depende del que hayamos pueston en el archivo Index.js.

Estos pasos se deben repetir cada vez que actualicemos los archivos de la practica, pero tambien existe una alternativa la cual nos permite que la terminal vaya actualizando automaticamente la practica.

Para realizar esta alternativa deberemos escribir lo siguiente en la terminal: "npm start".
 
### Analice las pruebas end-to-end 🔩
 
_Explica qué verifican estas pruebas y por qué_
 
```
Proporciona un ejemplo
```
 
### Y las pruebas de estilo de codificación ⌨️
 
_Explica qué verifican estas pruebas y por qué_
 
```
Proporciona un ejemplo
```
 
## Despliegue 📦
 
_Agrega notas adicionales sobre cómo hacer deploy_
 
## Construido con 🛠️
  
* [Bitbucket](http://bitbucket.org/) - El repositorio online utilizado.
 
## Versionado 📌
 
Para todas las versiones disponibles, mira los [tags en este repositorio](https://bitbucket.org/marioml7902/practica-sd/commits/).
 
## Autores ✒️
 
* **Mario Martínez** - [marioml7902](#https://bitbucket.org/marioml7902)
 
## Licencia 📄
 
Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles