'use strict'

const port = process.env.PORT || 3000;

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const req = require('express/lib/request');

const app = express();

var db = mongojs('SD');
var id = mongojs.ObjectID;

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}))
app.use(express.json());


//middlewares

var allowMethods = (req, res, next) => {
    res.header("Acces-Control-Allow-Methods", "GET, MOST, PUT, DELETE, OPTION");
    return next();
};

var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "token");
    return next();
}

var auth = (req, res, next) => {
    if(req.headers.token === "password1234") {
        return next();
    } else{
        return next(new Error("No autorizado"));
    };
};

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.use(allowMethods);
app.use(allowCrossTokenHeader);

app.param("coleccion", (req, res, next, coleccion) => {
    req.collection = db.collection(coleccion);
    return next();
});

//rutas
app.param("coleccion", (req, res, next, coleccion) => {
    req.collection = db.collection(coleccion);
    return next();
});

app.get('/api', (req, res, next) => {
    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);
    });
});

app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/:coleccion', auth, (req, res, next) => {
    const elemento = req.body;

    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } else {
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
    req.collection.update({_id: id(elementoId)}, {$set: req.body},
                           {safe: true, multi: false}, (err, result) => {
                if (err) return next(err);
                res.json(result);

    });
});

app.delete('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;

    req.collection.remote({_id: id(elementoId)}, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST CRUD ejecutandose en https://localhost:${port}/api/:coleccion/:id`);
});


